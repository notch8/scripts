require 'rubygems'
require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("https://www.site24x7.com/api/end_maintenance")

http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
request = Net::HTTP::Post.new(uri.request_uri)

request.add_field("Content-Type", "application/json;charset=UTF-8")
request.add_field("Authorization", "")
request.add_field("Accept", "application/json; version=2.0")


data = {
  "maintenance_id"     => "",
  "monitors"           => [],
}

request.body = data.to_json

response = http.request(request)

puts response.body
