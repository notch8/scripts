require 'rubygems'
require 'net/http'
require 'uri'
require 'json'

uri = URI.parse("https://www.site24x7.com/api/start_maintenance")

http = Net::HTTP.new(uri.host, uri.port)
http.use_ssl = true
request = Net::HTTP::Post.new(uri.request_uri)

request.add_field("Content-Type", "application/json;charset=UTF-8")
request.add_field("Authorization", "")
request.add_field("Accept", "application/json; version=2.0")


data = {
  "selection_type"     => 2,
  "monitors"           => [],
  "perform_monitoring" => true,
  "duration"           => 15
}

request.body = data.to_json

response = http.request(request)

puts response.body
