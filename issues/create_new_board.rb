require 'rubygems'
require 'net/http'
require 'uri'
require 'json'

gitlab_token = ""
gitlab_project_id = ""
board_id = ""

gitlab_labels_uri = URI.parse("https://gitlab.com/api/v4/projects/#{gitlab_project_id}/labels")
http = Net::HTTP.new(gitlab_labels_uri.host, gitlab_labels_uri.port)
http.use_ssl = true
request = Net::HTTP::Get.new(gitlab_labels_uri.request_uri)
request.add_field("PRIVATE-TOKEN", gitlab_token)
response = http.request(request)
labels = JSON.parse(response.body)

gitlab_board_uri = URI.parse("https://gitlab.com/api/v4/projects/#{gitlab_project_id}/boards/#{board_id}/lists")
http = Net::HTTP.new(gitlab_board_uri.host, gitlab_board_uri.port)
http.use_ssl = true
request = Net::HTTP::Post.new(gitlab_board_uri.request_uri)
request.add_field("PRIVATE-TOKEN", gitlab_token)
request.add_field("Content-Type", "application/json;")
labels.each do |list|
  data = {
    "label_id" => list["id"]
  }.to_json
  request.body = data
  response = http.request(request)
  puts response.body
end
