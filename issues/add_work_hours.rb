require 'rubygems'
require 'net/http'
require 'uri'
require 'json'
require 'pry'
require_relative 'requests'

include Requests

authtoken = ""
gitlab_token = ""
portal_id = ""
zoho_project_id = ""
gitlab_project_id = ""

# Gets zoho tasks in @task variable
get_zoho_tasks(authtoken, portal_id, zoho_project_id)

# Gets gitlab issues in @issues variable
get_gitlab_issues(gitlab_token, gitlab_project_id)


@issues.flatten.each do |issue|
  gitlab_uri = URI.parse("https://gitlab.com/api/v4/projects/5735235/issues/#{issue["iid"]}/add_spent_time")
  http = Net::HTTP.new(gitlab_uri.host, gitlab_uri.port)
  http.use_ssl = true
  request = Net::HTTP::Post.new(gitlab_uri.request_uri)
  request.add_field("PRIVATE-TOKEN", gitlab_token)
  request.add_field("Content-Type", "application/json;")
  @tasks.flatten.each do |hours|
    if hours["work"] != "0:00" && hours["work"] != "-"
      formatted_hours = format_time(hours["work"])
      data = {
        "duration" => formatted_hours,
      }.to_json
      request.body = data
      response = http.request(request)
      puts response.body
    end
  end
end
