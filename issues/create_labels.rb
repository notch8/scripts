require 'rubygems'
require 'net/http'
require 'uri'
require 'json'
require_relative 'colors'
require_relative 'requests'

include ColorArray
include Requests

authtoken = ""
gitlab_token = ""
zoho_project_id = ""
portal_id = ""

# Gets zoho tasklists in @tasklists variable
get_zoho_tasklists(authtoken, portal_id, zoho_project_id)


gitlab_uri = URI.parse("https://gitlab.com/api/v4/projects/5735235/labels")
http = Net::HTTP.new(gitlab_uri.host, gitlab_uri.port)
http.use_ssl = true
request = Net::HTTP::Post.new(gitlab_uri.request_uri)
request.add_field("PRIVATE-TOKEN", gitlab_token)
request.add_field("Content-Type", "application/json;")
@tasklists.each do |tasklist|
  data = {
    "name"   => tasklist["name"],
    "color"  => COLORS.sample
  }.to_json
  request.body = data
  response = http.request(request)
  puts response.body
end
