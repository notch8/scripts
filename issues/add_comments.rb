require 'rubygems'
require 'net/http'
require 'uri'
require 'json'
require 'pry'
require 'time'
require_relative 'requests'

include Requests

authtoken = ""
gitlab_token = ""
portal_id = ""
zoho_project_id = ""
gitlab_project_id = ""

# Gets zoho tasks in @task variable
get_zoho_tasks(authtoken, portal_id, zoho_project_id)

# Gets gitlab issues in @issues variable
get_gitlab_issues(gitlab_token, gitlab_project_id)

@issues.flatten.each do |issue|
  gitlab_uri = URI.parse("https://gitlab.com/api/v4/projects/5735235/issues/#{issue["iid"]}/notes")
  http = Net::HTTP.new(gitlab_uri.host, gitlab_uri.port)
  http.use_ssl = true
  request = Net::HTTP::Post.new(gitlab_uri.request_uri)
  request.add_field("PRIVATE-TOKEN", gitlab_token)
  request.add_field("Content-Type", "application/json;")
  @tasks.flatten.each do|comment|
    begin
      has_comment = comment["details"]["comments"]
    rescue
      has_comment = nil
    end
    if issue["title"] == comment["name"] && has_comment
      comment["details"]["comments"].each do |c|
      data = {
        "body"  => "#{c["content"]} -created at #{c["created_time_format"]}",
        "created_at"  => Time.strptime(c["created_time_format"], '%m-%d-%Y %I:%M:%S %p').iso8601
      }.to_json
      request.body = data
      response = http.request(request)
      puts response.body
      end
    end
  end
end
