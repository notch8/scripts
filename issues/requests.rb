module Requests

  def get_zoho_tasks(authtoken, portal_id, zoho_project_id)
    @tasks = []
    index = 1
    # breaks if less than 100 because return limit is 100 per page
    loop do
      params = {authtoken: authtoken, index: index, range: 100}
      zoho_uri = URI.parse("https://projectsapi.zoho.com/restapi/portal/#{portal_id}/projects/#{zoho_project_id}/tasks/")
      zoho_uri.query = URI.encode_www_form(params)
      http = Net::HTTP.new(zoho_uri.host, zoho_uri.port)
      http.use_ssl = true
      request = Net::HTTP::Get.new(zoho_uri.request_uri)
      response = http.request(request)
      data = JSON.parse(response.body)
      @tasks << data["tasks"]
      index += 100
      break if data["tasks"].count < 100
    end
  end

  def get_zoho_tasklists(authtoken, portal_id, zoho_project_id)
    params = {authtoken: authtoken, flag: "internal"}
    zoho_uri = URI.parse("https://projectsapi.zoho.com/restapi/portal/#{portal_id}/projects/#{zoho_project_id}/tasklists/")
    zoho_uri.query = URI.encode_www_form(params)
    http = Net::HTTP.new(zoho_uri.host, zoho_uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(zoho_uri.request_uri)
    response = http.request(request)
    data = JSON.parse(response.body)

    @tasklists = data["tasklists"]
  end

  def get_zoho_users(authtoken, portal_id, zoho_project_id)
    params = {authtoken: authtoken}
    zoho_uri = URI.parse("https://projectsapi.zoho.com/restapi/portal/#{portal_id}/projects/#{zoho_project_id}/users/")
    zoho_uri.query = URI.encode_www_form(params)
    http = Net::HTTP.new(zoho_uri.host, zoho_uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(zoho_uri.request_uri)
    response = http.request(request)
    data = JSON.parse(response.body)
    @zoho_users = data["users"]
  end

  def get_gitlab_issues(gitlab_token, gitlab_project_id)
    @issues = []
    page = 1
    # breaks if less than 100 because return limit is 100 per page
    loop do
      params_issues = {per_page: 100, page: page}
      gitlab_issues_uri = URI.parse("https://gitlab.com/api/v4/projects/#{gitlab_project_id}/issues")
      gitlab_issues_uri.query = URI.encode_www_form(params_issues)
      http = Net::HTTP.new(gitlab_issues_uri.host, gitlab_issues_uri.port)
      http.use_ssl = true
      request = Net::HTTP::Get.new(gitlab_issues_uri.request_uri)
      request.add_field("PRIVATE-TOKEN", gitlab_token)
      response = http.request(request)
      data = JSON.parse(response.body)
      @issues << data
      page += 1
      break if data.count < 100
    end
  end

  def get_gitlab_users(gitlab_token)
    gitlab_useres_uri = URI.parse("https://gitlab.com/api/v4/groups/1050736/members")
    http = Net::HTTP.new(gitlab_useres_uri.host, gitlab_useres_uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(gitlab_useres_uri.request_uri)
    request.add_field("PRIVATE-TOKEN", gitlab_token)
    response = http.request(request)
    data = JSON.parse(response.body)
    @gitlab_users = data
  end

  # Formats time for work hours
  def format_time(time)
    time.slice!("/ day")
    if time.include?("m")
      time.gsub(/:/, 'h').delete(' ')
    else
      time.gsub(/:/, 'h').concat('m').delete(' ')
    end
  end

  #Assigns users from zoho to gitlab
  def join_users(issue)
    @assignee_ids = []
    check_unassign = issue["details"]["owners"][0]["name"]
    owners = issue["details"]["owners"]
    if check_unassign != "Unassigned"
      owners.each do |owner|
        @gitlab_users.each do |user|
          if user["name"].downcase.include?(owner["name"].downcase)
            @assignee_ids << user["id"]
          end
        end
      end
    end
  end

end
