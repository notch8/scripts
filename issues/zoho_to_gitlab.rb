require 'rubygems'
require 'net/http'
require 'uri'
require 'json'
require 'pry'
require_relative 'requests'

include Requests

authtoken = ""
gitlab_token = ""
portal_id = ""
zoho_project_id = ""
gitlab_project_id = ""

# Gets zoho tasks in @task variable
get_zoho_tasks(authtoken, portal_id, zoho_project_id)

# Gets zoho users in @zoho_users variable
get_zoho_users(authtoken, portal_id, zoho_project_id)

# Gets zoho users in @gitlab_users variable
get_gitlab_users(gitlab_token)

gitlab_uri = URI.parse("https://gitlab.com/api/v4/projects/5735235/issues")
http = Net::HTTP.new(gitlab_uri.host, gitlab_uri.port)
http.use_ssl = true
request = Net::HTTP::Post.new(gitlab_uri.request_uri)
request.add_field("PRIVATE-TOKEN", gitlab_token)
request.add_field("Content-Type", "application/json;")
@tasks.flatten.each do |issue|
  join_users(issue) #returns @assignee_ids
  binding.pry
  data = {
    "title"        => issue["name"],
    "description"  => "#{issue["description"]} -created at #{issue["created_time_format"]}",
    "labels"       => issue["tasklist"]["name"],
    "assignee_ids" => @assignee_ids,
    "created_at"   => Time.at(issue["created_time_long"]).strftime('%Y-%m-%dT%H:%M:%S.%L%z')
  }.to_json
  request.body = data
  response = http.request(request)
  puts response.body
end
